/* Function hover submenu */
function hover() {
	$('.sub-menu').hide();
	$('.dropdown-menu').hover(function() {

		// $('.sub-menu').slideToggle("slow");
		// $('.sub-menu').slideDown("slow");
		$('.sub-menu').toggleClass('box-submenu');
	});
	
}

/* Function click scroll down */
function click_scroll() {
	$('.scroll-in a').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
	        || location.hostname == this.hostname) {

	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	        if (target.length) {
	            $('html,body').animate({
	              scrollTop: target.offset().top - '0'
	            }, 1000);
	            return false;
	        }
	    }
    });
}

/* Function click scroll Top */
function click_top() {
	$('#back-top a').click(function(event) {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}
/* Function click show menu mobi */
function show_menu() {
	// $('.mega-menu').hide();
	$('.ic-menu').click(function(event) {
		
		$('.mega-menu').slideToggle('slow');
		// $('.mega-menu').toggleClass('active-menu');
	});
	
}

/**/
function click_toggle() {
	$('.view-close').click(function(event) {
		var in_blog = $(this).parent().parent();
		var key_blog = in_blog.index();
		console.log('in_blog');

		$('.blog').eq(key_blog).slideToggle('slow');
		// $('.blog').eq(key_blog).toggleClass('active');
		$(this).toggleClass('active');

	})
}


$(document).ready(function() {
	hover();
	click_scroll();
	click_top();
	show_menu();
	click_toggle();
	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        effect: 'flip',
        grabCursor: true,
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });
	
});

